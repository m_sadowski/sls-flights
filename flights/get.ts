'use strict'

import * as request from 'request'
import * as _ from 'underscore'
import { DynamoDB } from 'aws-sdk'

const dynamoDb = new DynamoDB.DocumentClient()

interface FlightsQueryParams {
  callsign: string
}

class FlightsService {

  public async getAll (queryParams: FlightsQueryParams, func?: Function): Promise<Object> {
    const zonesData = await this.getFrZonesData(this.calculateZones(90, 90), queryParams)
    return this.getFlightsList(zonesData, func)
  }

  public getFlightDetails (flightId: string): Promise<Object> {
    return new Promise(async (resolve, reject) => {
      const detailsUrl: string = `https://data-live.flightradar24.com/clickhandler/?version=1.5&flight=${flightId}`;
      request(
        detailsUrl,
        { json: true },
        (err, res, body) => {
          if (err) {
            reject(err)
            return
          }
          resolve(body)
        })
    });
  }

  private calculateZones(offsetX: number, offsetY: number): Array<Array<number>> {
    const zones: Array<Array<number>> = []
    for (let y: number = -90; y <= 90 - offsetY; y += offsetY) {
      for (let x: number = -180; x <= 180 - offsetX; x += offsetX) {
        zones.push([y + offsetY, y, x, x + offsetX])
      }
    }
    return zones
  }

  private getFrZoneData(zone: Array<number>, queryParams: FlightsQueryParams): Promise<any> {
    return new Promise((resolve, reject) => {
      let zoneUrl: string = `https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=${zone[0]},${zone[1]},${zone[2]},${zone[3]}&faa=1&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&stats=1`

      if (queryParams.callsign) {
        zoneUrl += `&callsign=${queryParams.callsign.toUpperCase()}`
      } else {
        reject(new Error('GetFrZoneData - callsign not provided.'))
      }
      request(
        zoneUrl,
        { json: true },
        (err, res, body) => {
          if (err) {
            reject(err)
            return
          }
          // let zoneData = _.omit(body, ['full_count', 'version', 'stats', 'selected-aircraft'])
          delete body['full_count']
          delete body['version']
          delete body['stats']
          delete body['selected-aircraft']

          resolve(body)
        })
    })
  }

  private getFrZonesData(zonesArr: Array<Array<number>>, queryParams: FlightsQueryParams): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const frZonePromises: Array<any> = []
      zonesArr.forEach((zone) => {
        frZonePromises.push(this.getFrZoneData(zone, queryParams))
      })
      const allZonesDataArr: Array<any> = await Promise.all(frZonePromises)
      const allZonesData = _.extend({}, ...allZonesDataArr)
      // const allZonesData = { ...allZonesDataArr }
      resolve(allZonesData)
    })
  }

  private getFlightsList(zoneData, func?: Function): Object {
    const flights: Object = {}
    Object.keys(zoneData).forEach(key => {
      const val: any = zoneData[key]
      if (Array.isArray(val) && val.length >= 14) {
        let flight = {
          id: key,
          cs: val[13],
          fr: val[11],
          to: val[12],
          ac: val[8], // ac to aircraft !!!
          rg: val[9]
        }
        if (func) {
          flight = func(flight)
        }
        flights[flight.id] = flight
      }
    })
    return flights
  }
}

const validateFlight = (flight) => {

  // walidacja, czy rekord nadaje sie do zapisu w bazie
  if (!flight.id || !flight.cs || !flight.fr || !flight.to || !flight.ac || !flight.rg) {
    return false
  }
  return true
}

const putFlight = (flight, tableName) => {
  return new Promise(async (resolve, reject) => {
    const timestamp = new Date().getTime()
    const putParams = {
      TableName: tableName,
      Item: {
        ...flight,
        cr: timestamp,
        up: timestamp
      }
    }
    try {
      const putResult = await dynamoDb.put(putParams).promise()
      resolve(putResult)
    } catch (err) {
      reject(err)
    }
  })
}

const updateFlight = (flight, tableName) => {
  // dodaje rekord (jesli brak id) lub edytuje
  return new Promise(async (resolve, reject) => {
    const timestamp = new Date().getTime()
    const updateParams = {
      TableName: tableName,
      Key: {
        id: flight.id
      },
      UpdateExpression: 'set c = :c, f = :f, t = :t, a = :a, r = :r, u = :u',
      ExpressionAttributeValues: {
        ':c': flight.cs,
        ':f': flight.fr,
        ':t': flight.to,
        ':a': flight.ac,
        ':r': flight.rg,
        ':u': timestamp
      },
      ReturnValues: 'UPDATED_NEW'
    }
    try {
      const updateResult = await dynamoDb.update(updateParams).promise()
      resolve(updateResult)
    } catch (err) {
      reject(err)
    }
  })
}

const updateFlightDetails = (flightId, details, tableName) => {
  // dodaje rekord (jesli brak id) lub edytuje

  const timeScheduled = details.time.scheduled
  const timeScheduledDeparture = (timeScheduled && timeScheduled.departure) ? timeScheduled.departure : 0
  const timeScheduledArrival = (timeScheduled && timeScheduled.arrival) ? timeScheduled.arrival : 0

  const timeReal = details.time.real
  const timeRealDeparture = (timeReal && timeReal.departure) ? timeReal.departure : 0
  const timeRealArrival = (timeReal && timeReal.arrival) ? timeReal.arrival : 0

  const timeEstimated = details.time.estimated
  const timeEstimatedArrival = (timeEstimated && timeEstimated.arrival) ? timeEstimated.arrival : 0

  const statusColor = details.status.generic.status.color
  let status = 0
  switch (statusColor) {
    case 'green':
      status = 1
      break
    case 'yellow':
      status = 2
      break
    case 'red':
      status = 3
      break
    default:
      status = 0
  }

  return new Promise(async (resolve, reject) => {
    const timestamp = new Date().getTime()
    const updateParams = {
      TableName: tableName,
      Key: {
        id: flightId
      },
      UpdateExpression: 'set u = :u, tsd = :tsd, tsa = :tsa, trd = :trd, tra = :tra, tea = :tea, st = :st',
      ExpressionAttributeValues: {
        ':u': timestamp,
        ':tsd': timeScheduledDeparture,
        ':tsa': timeScheduledArrival,
        ':trd': timeRealDeparture,
        ':tra': timeRealArrival,
        ':tea': timeEstimatedArrival,
        ':st': status
      },
      ReturnValues: 'UPDATED_NEW'
    }
    try {
      const updateResult = await dynamoDb.update(updateParams).promise()
      resolve(updateResult)
    } catch (err) {
      reject(err)
    }
  })
}

module.exports.get = async (event, context, callback) => {

  // getAll, potem upsert do bazy po id, id to id lotu z flightradar (hash jakis 8 czy 10 znakow)
  const flightsService = new FlightsService()
  const flights = await flightsService.getAll({ callsign: 'lot' }) // klucz to id lotu
  const flightsCount = Object.keys(flights).length
  console.log(`Flights count: ${flightsCount}`)

  let putCount = 0

  for (const flightId in flights) {
    try {
      const flight = flights[flightId]
      if (validateFlight(flight)) {
        const putResult = await updateFlight(flight, process.env.DYNAMODB_TABLE)
        putCount++
      }
    } catch (err) {
      console.error(err)
    }
  }

  // return putCount
  callback(null, `${putCount}`)
}

module.exports.details = async (event, context, callback) => {
  // trigger wywolywany podczas dodawania/edycji/usuwania rekordow z tabeli

  const flightsService = new FlightsService()

  event.Records.forEach(async (record) => {
    // console.log('Stream record: ', JSON.stringify(record, null, 2))
    if (record.eventName === 'INSERT') {
      const flightId = record.dynamodb.Keys.id.S
      console.log(`Getting details of ${flightId}`)

      let details = null;
      try {
        details = await flightsService.getFlightDetails(flightId)
      } catch (err) {
        console.error(err)
      }

      if (details) {
        // update tabeli
        try {
          const updateResult = await updateFlightDetails(flightId, details, process.env.DYNAMODB_TABLE)
          console.log(`Details for ${flightId} updated successfully.`)
        } catch (err) {
          console.error(err)
        }
      }
    }
  })
}
